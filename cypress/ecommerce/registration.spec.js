describe("Test 1", () => {
  it("should fill out the form, press 'Comenzar', and validate the redirection and final URL", () => {
    cy.visit("https://app.rocketfy.co/signup");

    // Function to generate random numbers
    function generateRandomNumber() {
      return Math.floor(Math.random() * 1000);
    }

    // Generate random values for firstname, lastname, and email
    const randomFirstName = "usertest" + generateRandomNumber();
    const randomLastName = "lastnametest" + generateRandomNumber();

    // Generate random email and save to environment
    const randomEmail = "usertestqa" + generateRandomNumber() + "@gmail.com";
    Cypress.env("randomEmail", randomEmail);

    // Input values
    const nameStore = "testnewqa1";
    const phone = "1122334455";

    // Fill out the form with random values
    cy.get('input[formcontrolname="firstName"]').click().type(randomFirstName);
    cy.get('input[formcontrolname="lastName"]').click().type(randomLastName);
    cy.get('input[formcontrolname="nameStore"]').click().type(nameStore);

    // Click the button with width 100% and validate the text and width
    cy.contains("Continuar").click();

    // Click the desired list items
    cy.contains(" No, es mi primera vez ").click();
    cy.contains(" Vender mis propios productos ").click();
    cy.contains(" Aún no vendo ").click();
    cy.contains(" No ").click();
    cy.contains("En un sitio web").click();

    // Fill out additional inputs with random email
    cy.get('input[formcontrolname="email"]').click().type(randomEmail);
    cy.get('input[formcontrolname="phone"]').click().type(phone);
    cy.get('input[formcontrolname="pass"]').click().type("pass2023!"); // Fixed password

    // Click the images
    cy.get("img.avisador3").click();

    // Validate the "Comenzar" button width is 100% and click
    cy.contains("Comenzar").click();

    cy.wait(22000);

    // Wait for the URL to change to "https://app.rocketfy.co/feed?register=true"
    cy.url().should("eq", "https://app.rocketfy.co/feed");

    // Now the website has loaded completely
    // Continue with further test steps if needed.

    // Check if the user is on the final URL "https://app.rocketfy.co/feed"
    cy.url().should("eq", "https://app.rocketfy.co/feed");
  });
});
