describe("Test 2 Page", () => {
  it("should perform the specified steps", () => {
   // Get the user and password generated in the first test case
   const randomEmail = Cypress.env("randomEmail");

   // Visit the login page
   cy.visit("https://app.rocketfy.co/login");

   // Fill out the email input with the random email
   cy.get('input#user').click().type(randomEmail);
   
   // Fill out the password input with "pass2023"
   cy.get('input[type="password"]').type("pass2023");

   // Click the button with label "Entrar"
   cy.contains("Entrar").click();

   // Wait for 15 seconds
   cy.wait(15000);

   // Go to https://app.rocketfy.co/ajustes/direcciones
   cy.visit("https://app.rocketfy.co/ajustes/direcciones");
    // Click on the button with text "Nueva dirección"
    cy.contains("Nueva dirección").click();

    // Fill out the input with placeholder "Elije un nombre para tu nueva dirección"
    cy.get('input[placeholder="Elije un nombre para tu nueva dirección"]').click().type("Test Address");

    // Click on the department selector and select "Arauca"
    cy.get('ng-select[placeholder="Selecciona tu departamento"]').click();
    cy.get('ng-option-label').contains('Arauca').click();

    // Click on the city selector and select "Fortul"
    cy.get('div[placeholder="Selecciona tu ciudad"]').click();
    cy.get('ng-option-label').contains('Fortul').click();

    // Fill out the input with placeholder "Calle 99 sur"
    cy.get('input[placeholder="Calle 99 sur"]').click().type("Calle 99 sur");

    // Fill out the input with placeholder "99"
    cy.get('input[placeholder="99"]').click().type("99");

    // Fill out the input with label "Barrio/Apto/Unidad"
    cy.contains("Barrio/Apto/Unidad").siblings('input').click().type("Test");

    // Click on the button with text "Crear ubicación"
    cy.contains("Crear ubicación").click();
  });
});
