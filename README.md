# Project Name: Automation-Platform-Site // Rocketfy.co - Cypress Automation

![Rocketfy.co Logo](https://fs.rocketfy.co/images/partners/rocketfy.svg)

## Description

This project contains automated tests using [Cypress](https://www.cypress.io/) for the Rocketfy.co platform. Rocketfy.co is a productivity and task management platform designed to help teams stay organized and collaborate effectively.

The purpose of this repository is to ensure the reliability and stability of the Rocketfy.co application by automating the testing process. Cypress is used as the testing framework due to its simplicity, fast execution, and robust capabilities for end-to-end testing.

## Table of Contents

- [Description](#description)
- [Installation](#installation)
- [Running Tests](#running-tests)
- [Documentation](#documentation)
- [Author](#author)
- [License](#license)

## Installation

To get started with the Cypress automation for Rocketfy.co, follow these steps:

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/apiccinellif/automation-platform-site.git
   cd automation-platform.site

2. Install the project dependencies:
    ``bash
    Copy code
    npm install

## Running Tests

To execute the Cypress tests for Rocketfy.co, use the following command:

    ``bash
    Copy code
    npm run cy:run

This command will run the Cypress tests in headless mode, providing you with the test results and any potential failures.

If you prefer to run the tests in interactive mode, use:

    ``bash
    Copy code
    npm run cy:open



## Documentation

For comprehensive documentation on Cypress and how to write and manage test scripts, refer to the official Cypress documentation:

[Cypress Documentation](https://docs.cypress.io/)

## Author

This Cypress automation project for Rocketfy.co is developed and maintained by Andrés Piccinelli.

Connect with Andrés on [LinkedIn](https://www.linkedin.com/in/andrespiccinelli/) to stay updated on his latest projects and professional updates.

## License

This project is licensed under the [MIT License](LICENSE). Feel free to use, modify, and distribute it as per the terms of the license.
