const { defineConfig } = require("cypress");

module.exports = defineConfig({
  e2e: {
    specPattern: "cypress/ecommerce/**/*.spec.js",
    setupNodeEvents(on, config) {
      // Implement node event listeners specific to "e2e" here
    },
    // Other specific configurations for "e2e" go here...
  },

  // Other shared configurations go here...
});
